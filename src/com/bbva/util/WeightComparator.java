package com.bbva.util;

import java.util.Comparator;

import static com.bbva.util.StringManipulator.getWeight;

public class WeightComparator implements Comparator<String>
{
    @Override
    public int compare(String s1, String s2)
    {
        if (s1 == null || s2 == null)
            return 0;
        try
        {
        	String[] s1Split = s1.split("-");
        	String[] s2Split = s2.split("-");
            if (s1Split[1].equals(s2Split[1]))
            	return s1Split[0].compareTo(s2Split[0]);
            return s1Split[1].compareTo(s2Split[1]);
        }
        catch (Exception e)
        {
            return 0;
        }
    }
}
