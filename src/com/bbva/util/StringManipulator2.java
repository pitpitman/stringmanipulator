package com.bbva.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.annotation.*;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
//import java.util.concurrent.*;
//import java.util.regex.Pattern;

@ClassPreamble
public class StringManipulator2
{
//	private static Pattern p = Pattern.compile("[a-zA-Z]+");
//	private static ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
	
	@MethodPreamble (
		purpose = "Removes all non-alphabetic characters from a string and converts all characters to upper case.",
		parameters = {"<String>"},
		returns = "Cleaned <String>"
	)
	public static String cleanString(String str)
	{
		if (str == null || str == "")
			return "";
//		if (p.matcher(str).matches())
//			return str.toUpperCase();
		StringBuilder sb = new StringBuilder();
		char[] chars = str.toCharArray();
		for (char c : chars)
		{
			c = Character.toUpperCase(c);
			if (!(65 <= (int)c && (int)c <= 90))
				continue;
			sb.append(c);
		}
		return sb.toString();
//		return str.replaceAll("[^a-zA-Z]", "").toUpperCase();
	}
	
	@MethodPreamble (
		purpose = "Check if a word only contains unique characters, excluding non-alphabetic characters and not case sensitive.",
		parameters = {"<String>"},
		returns = "<boolean> true only if the word doesn't have any character that is repeated, <boolean> false otherwise."
	)
	public static boolean hasUniqueChars(String word)
	{
		if (word == null || word == "" || word.length() > 26)
			return false;
		BitSet seen = new BitSet(), repeated = new BitSet();
		char[] chars = word.toCharArray();
		for (char c : chars)
		{
			c = Character.toUpperCase(c);
			if (!(65 <= (int)c && (int)c <= 90))
				continue;
			(seen.get(c)? repeated: seen).set(c);
			if (!repeated.isEmpty())
				return false;
		}
		return true;
	}
	
	@MethodPreamble (
		purpose = "Check the weight of a string. " +
	              "The weight is defined as a numeric value that is the average of each char ascii value contained in the string, " +
				  "excluding non-alphabetic characters and not case sensitive.",
		parameters = {"<String>"},
		returns = "<double> the average of each char ascii value in the <String>."
	)
	public static double getWeight(String word)
	{
		if (word == null || word == "")
			return 0;

//		int parallelismThreshold = 50000;

		char[] chars = word.toCharArray();
//		if (chars.length < parallelismThreshold)
//		{
			int cumul = 0;
			int ct = 0;
			for (char c : chars)
			{
				c = Character.toUpperCase(c);
				if (!(65 <= (int)c && (int)c <= 90))
					continue;
				cumul += (int)c;
				++ct;
			}
			return cumul/ct;
//		}
//		else
//		{
//			int numThreads = ((chars.length % parallelismThreshold) == 0) ? (chars.length / parallelismThreshold) : ((chars.length / parallelismThreshold) + 1);
//			ExecutorService executor = Executors.newFixedThreadPool(numThreads);
//			List <Future<List<Integer>>> futures = new ArrayList<Future<List<Integer>>>();
//			int wordLength = word.length();
//			for (int i = 0; i < numThreads; i++)
//			{
//				int endInd = (i+1)*parallelismThreshold;
//				Future<List<Integer>> future = executor.submit(new CallableAdder(word.substring(i*parallelismThreshold, (endInd > wordLength) ? wordLength : endInd).toCharArray()));
//				futures.add(future);
//			}
//			int cumul = 0;
//			int ct = 0;
//			for (Future<List<Integer>> fut : futures)
//			{
//				try
//				{
//					cumul += fut.get().get(0);
//					ct += fut.get().get(1);
//				}
//				catch (InterruptedException e)
//				{
//
//				}
//				catch (ExecutionException e)
//				{
//
//				}
//			}
//			return cumul/ct;
//		}
	}
	
	@MethodPreamble (
		purpose = "Sort a large array of strings based on the value obtained by the weight function in ascending order." +
	              "The resulting sorted array of <String>s will be written to file.",
		parameters = {"<String> []"},
		returns = "<List<String>> the sorted list of <String>s."
	)
	public static List<String> sortStrings(String[] words)
	{
		if (words == null || words.length == 0)
			return null;

		TreeSet<String> sortingSet = new TreeSet<>(new WeightComparator());

		for (String str : words)
		{
			str = cleanString(str);
			double wt = getWeight(str);
			str = str + "-" + Double.toString(wt);
			sortingSet.add(str);
		}
		return new ArrayList<>(sortingSet);
	}
	
	@MethodPreamble (
		purpose = "Read from file and call methods herein: <hasUniqueChars> and sort all words in ascending order " +
	              "by string weight and output the cleaned sorted list alphabetically in all upper case.",
		parameters = {"<String> []"}
	)
	public static void main(String... args)
	{
//		System.out.println(System.currentTimeMillis());
//		for (int i = 0; i < 1000; i++)
//		System.out.println(StringManipulator.cleanString("abc123tre*Au"));
//		System.out.println(System.currentTimeMillis());
//		System.out.flush();

//		System.out.println(StringManipulator.hasUniqueChars("abc123tre*u"));
//		System.out.flush();

//		System.out.println(System.currentTimeMillis());
//		for (int i = 0; i < 1000; i++)
//		System.out.println(StringManipulator.hasUniqueChars("abc123tre*Au"));
//		System.out.println(System.currentTimeMillis());
//		System.out.flush();
		
//		System.out.println(StringManipulator.hasUniqueChars("a2p-}}pl66e"));
//		System.out.flush();
		
//		System.out.println(StringManipulator.hasUniqueChars("co%m$Puter"));
//		System.out.flush();

//		System.out.println(System.currentTimeMillis());
//		for (int i = 0; i < 1000; i++)
//		System.out.println(StringManipulator.getWeight("Ab5-:C"));
//		System.out.println(System.currentTimeMillis());
//		System.out.flush();
		
//		System.out.println(StringManipulator.getWeight("ab5-:C"));
//		System.out.flush();

//		String testStr = "Pellentesque sit amet nunc eu diam eleifend pharetra vitae eget odio. Sed mollis sodales iaculis. Praesent in auctor libero. Quisque sagittis mauris elit, in tempor enim vehicula non. Phasellus hendrerit turpis enim, id aliquet elit pulvinar pulvinar. Proin iaculis quam quis nunc convallis posuere. Duis ac interdum lectus, id feugiat metus. Vestibulum luctus ultricies auctor. Praesent interdum, enim quis varius venenatis, lorem diam elementum risus, eget semper magna purus eget urna. Nunc vitae gravida nisi, sed aliquet nulla. Maecenas ac maximus purus. Curabitur magna nisl, sollicitudin quis eros vel, sagittis sollicitudin mauris. Sed pellentesque sodales est eu sollicitudin. Pellentesque aliquet justo ante, aliquet ultrices sem varius ac. Quisque viverra sit amet magna nec elementum.Aenean et odio gravida, aliquet lectus ac, eleifend urna. Duis in accumsan ligula. Morbi pretium odio eu vestibulum sollicitudin. Pellentesque eleifend molestie turpis nec vulputate. Vestibulum sagittis, urna sit amet laoreet porta, lorem odio dapibus purus, non sodales tortor orci vitae eros. Donec congue vitae enim quis sollicitudin. Suspendisse nulla felis, tincidunt tincidunt nisl vel, rhoncus bibendum ante. Nullam id faucibus ipsum. Mauris velit risus, finibus in nunc eget, venenatis eleifend mauris.Praesent suscipit quis arcu eu luctus. Curabitur non dolor vehicula purus placerat semper id et lacus. Ut sollicitudin nec diam quis porttitor. Nulla ullamcorper, metus at tempus fringilla, diam felis lacinia odio, venenatis rhoncus neque nisi sed ante. Duis euismod pretium mi, quis fermentum lacus condimentum sed. Ut et dui nibh. Morbi rhoncus massa venenatis magna tristique, a imperdiet tortor volutpat. Etiam in quam tincidunt, laoreet nulla eu, dictum arcu. Donec nec purus eu dui malesuada dictum. Mauris eu est sit amet lorem viverra tincidunt. Curabitur sodales tellus at lorem vestibulum, ac iaculis purus sodales.Ut ac purus dapibus, dignissim dolor eu, hendrerit orci. Quisque sollicitudin vestibulum posuere. Donec dui metus, tempor ut rutrum et, molestie at massa. In interdum tempor consectetur. Donec tincidunt, nisl nec vestibulum molestie, odio urna placerat sem, at elementum turpis est quis augue. Morbi eget neque a nibh porttitor posuere sit amet non dui. Vestibulum cursus sem sit amet nisi facilisis, et semper augue ultrices. Aenean porta, sapien et condimentum gravida, elit massa tincidunt nisl, vel congue enim sem et neque. Pellentesque eleifend ipsum eu ipsum molestie, in cursus tellus cursus. Donec sed diam ut nibh dignissim accumsan. Morbi pharetra, odio ac malesuada tempor, ex tellus dictum ipsum, vitae finibus velit lectus sit amet mauris.";
//		testStr = testStr + testStr;
//		System.out.println(System.currentTimeMillis());
//		for (int i = 0; i < 1000; i++)
//		System.out.println(StringManipulator.getWeight(testStr));
//		System.out.println(System.currentTimeMillis());
//		System.out.flush();

//		System.out.println(StringManipulator.sortStrings(new String[] {"co%m$Puter", "abc123tre*Au", "ab5-:C"}));
//		System.out.flush();

		String inFileName = "challenge_tests.csv";
		String outFileName = "challenge_sorted.csv";
		
		StringBuffer buffer = new StringBuffer();
		for (URL url :
		    ((URLClassLoader) (Thread.currentThread()
		        .getContextClassLoader())).getURLs()) {
			if (url.getPath().contains("StringManipulator"))
			{
				buffer.append(new File(url.getPath()));
				buffer.append(File.separator);
			}
		}
		String classpath = buffer.toString();
		classpath = classpath.substring(0, classpath.indexOf("bin" + File.separator)) + "src" + File.separator + "atdd" + File.separator;

		try (
				FileReader fileReader = new FileReader(classpath + inFileName);
				PrintWriter outFileWriter = new PrintWriter(new BufferedWriter(new FileWriter(classpath + outFileName)));
			)
		{
			BufferedReader br = new BufferedReader(fileReader);
			String line = null;
			ArrayList<String> lines = new ArrayList<>();
			br.readLine(); // skip header
			while ((line = br.readLine()) != null)
			{
				lines.add(line);
			}
			br.close();
			
			String[] words = lines.toArray(new String[0]);
			List<String> sortedWords = StringManipulator2.sortStrings(words);
			if (sortedWords == null)
			{
				System.out.println("Error sorting words.");
				System.out.flush();
				System.exit(1);
			}
			outFileWriter.println("Words,Unique,Weight");
			String firstWord = null;
			String secondWord = null;
			String[] splitWord = null;
			for (String word : sortedWords)
			{
				splitWord = word.split("-");
				firstWord = splitWord[0];
				secondWord = splitWord[1];
				outFileWriter.println(firstWord + "," + (StringManipulator2.hasUniqueChars(firstWord) ? "TRUE" : "FALSE") + "," + secondWord);
			}
			outFileWriter.flush();
		}
		catch (IOException | NoSuchElementException | IllegalStateException io)
		{
			System.out.println("IOException reading input file " + inFileName);
			System.out.println(io.getMessage());
			System.out.flush();
			System.exit(1);
		}
	}
	
}

@Documented
@interface ClassPreamble {
   String author() default "Terry Pitman";
   String date() default "01/25/2018";
   int currentRevision() default 1;
   String purpose() default "The fastest, most efficient implementation of palindrome-checking and string-sorting algorithms in Java";
}

@Documented
@interface MethodPreamble {
   String author() default "Terry Pitman";
   String date() default "01/25/2018";
   int currentRevision() default 1;
   String purpose() default "N/A";
   String[] parameters() default "N/A";
   String returns() default "void";
}
