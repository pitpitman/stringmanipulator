package com.bbva.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.annotation.*;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.stream.*;

@ClassPreamble
public class StringManipulator
{
	
	@MethodPreamble (
		purpose = "Removes all non-alphabetic characters from a string and converts all characters to upper case.",
		parameters = {"<String>"},
		returns = "Cleaned <String>"
	)
	public static String cleanString(String str)
	{
		if (str == null || str == "")
			return "";
		return str.chars()
	    .filter(i -> Character.isLetter(i))
	    .collect(StringBuilder::new, 
	         (StringBuilder sb, int c) -> sb.append(Character.toUpperCase((char) c)), 
	         StringBuilder::append).toString();
	}
	
	@MethodPreamble (
		purpose = "Check if a word only contains unique characters, excluding non-alphabetic characters and not case sensitive.",
		parameters = {"<String>"},
		returns = "<boolean> true only if the word doesn't have any character that is repeated, <boolean> false otherwise."
	)
	public static boolean hasUniqueChars(String word)
	{
		if (word == null || word == "")
			return false;
		
		BitSet seen = new BitSet(), repeated = new BitSet();
		word.chars()
		.mapToObj(i -> (char)i)
		.filter(i -> Character.isLetter(i))
		.forEachOrdered(c -> (seen.get(Character.toLowerCase(c))? repeated: seen).set(Character.toLowerCase(c)));
		
		if (repeated.isEmpty())
			return true;
		else
			return false;
	}
	
	@MethodPreamble (
		purpose = "Check the weight of a string. " +
	              "The weight is defined as a numeric value that is the average of each char ascii value contained in the string, " +
				  "excluding non-alphabetic characters and not case sensitive.",
		parameters = {"<String>"},
		returns = "<double> the average of each char ascii value in the <String>."
	)
	public static double getWeight(String word)
	{
		if (word == null || word == "")
			return 0;
		return word.chars()
				.mapToObj(i -> (char)i)
				.filter(i -> Character.isLetter(i))
				.collect(Collectors.averagingDouble(i -> (int)Character.toUpperCase(i)));
	}
	
	@MethodPreamble (
		purpose = "Sort a large array of strings based on the value obtained by the weight function in ascending order." +
	              "The resulting sorted array of <String>s will be written to file.",
		parameters = {"<String> []"},
		returns = "<List<String>> the sorted list of <String>s."
	)
	public static List<String> sortStrings(String[] words)
	{
		if (words == null || words.length == 0)
			return null;
		List<String> list = Arrays.asList(words).stream()
				.sorted((o1, o2)-> Double.valueOf(StringManipulator.getWeight(o1)).compareTo(Double.valueOf(StringManipulator.getWeight(o2))))
				.collect(Collectors.toList());
		return list;
	}
	
	@MethodPreamble (
		purpose = "Read from file and call methods herein: <hasUniqueChars> and sort all words in ascending order " +
	              "by string weight and output the cleaned sorted list alphabetically in all upper case.",
		parameters = {"<String> []"}
	)
	public static void main(String... args)
	{
		/*System.out.println(StringManipulator.cleanString("abc123tre*Au"));
		System.out.flush();
		
		System.out.println(StringManipulator.hasUniqueChars("abc123tre*u"));
		System.out.flush();
		
		System.out.println(StringManipulator.hasUniqueChars("abc123tre*Au"));
		System.out.flush();
		
		System.out.println(StringManipulator.hasUniqueChars("a2p�}}pl66e"));
		System.out.flush();
		
		System.out.println(StringManipulator.hasUniqueChars("co%m$Puter"));
		System.out.flush();
		
		System.out.println(StringManipulator.getWeight("Ab5-:C"));
		System.out.flush();
		
		System.out.println(StringManipulator.getWeight("ab5-:C"));
		System.out.flush();*/

		String inFileName = "challenge_tests.csv";
		String outFileName = "challenge_sorted.csv";
		
		StringBuffer buffer = new StringBuffer();
		for (URL url :
		    ((URLClassLoader) (Thread.currentThread()
		        .getContextClassLoader())).getURLs()) {
			if (url.getPath().contains("StringManipulator"))
			{
				buffer.append(new File(url.getPath()));
				buffer.append(File.separator);
			}
		}
		String classpath = buffer.toString();
		classpath = classpath.substring(0, classpath.indexOf("bin" + File.separator)) + "src" + File.separator + "atdd" + File.separator;

		try (
				InputStream fileInput = new FileInputStream(classpath + inFileName);
				PrintWriter outFileWriter = new PrintWriter(new BufferedWriter(new FileWriter(classpath + outFileName)));
			)
		{
			Scanner inScanner = new Scanner(fileInput);
			String line = null;
			ArrayList<String> lines = new ArrayList<>();
			if (inScanner.hasNext())
				inScanner.nextLine(); // skip header
			while ((inScanner.hasNext()) && (line = inScanner.nextLine()) != null)
			{
				lines.add(line);
			}
			inScanner.close();
			
			String[] words = lines.toArray(new String[0]);
			List<String> sortedWords = StringManipulator.sortStrings(words);
			if (sortedWords == null)
			{
				System.out.println("Error sorting words.");
				System.out.flush();
				System.exit(1);
			}
			outFileWriter.println("Words,Unique,Weight");
			for (String word : sortedWords)
			{
				outFileWriter.println(StringManipulator.cleanString(word) + "," + (StringManipulator.hasUniqueChars(word) ? "TRUE" : "FALSE") + "," + StringManipulator.getWeight(word));
			}
			outFileWriter.flush();
		}
		catch (IOException | NoSuchElementException | IllegalStateException io)
		{
			System.out.println("IOException reading input file " + inFileName);
			System.out.println(io.getMessage());
			System.out.flush();
			System.exit(1);
		}
	}
	
}

@Documented
@interface ClassPreamble {
   String author() default "Terry Pitman";
   String date() default "01/25/2018";
   int currentRevision() default 1;
   String purpose() default "The fastest, most efficient implementation of palindrome-checking and string-sorting algorithms in Java";
}

@Documented
@interface MethodPreamble {
   String author() default "Terry Pitman";
   String date() default "01/25/2018";
   int currentRevision() default 1;
   String purpose() default "N/A";
   String[] parameters() default "N/A";
   String returns() default "void";
}
