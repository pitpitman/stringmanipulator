package com.bbva.util;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

public class CallableAdder implements Callable<List<Integer>>
{
    char[] chars;

    CallableAdder (char[] chars)
    {
        this.chars = chars;
    }

    public List<Integer> call() throws Exception {
        if (chars == null) return Arrays.asList(0,0);
        int cumul = 0;
        int ct = 0;
        for (char c : chars)
        {
            c = Character.toUpperCase(c);
            if (!(65 <= (int)c && (int)c <= 90))
                continue;
            cumul += (int)c;
            ++ct;
        }
        return Arrays.asList(cumul, ct);
    }
}
